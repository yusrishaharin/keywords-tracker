import os
import sys
from ConfigParser import RawConfigParser


def parse_config():
    p = os.path
    path = p.abspath(p.join(p.abspath(__file__), '../config.ini'))

    if os.path.exists(path):
        print >>sys.stderr, '*** config.py: configuring from config.ini: %s' % repr(path)
    else:
        path = '/etc/kwtracter/config.ini'

    config = RawConfigParser()

    with open(path) as f:
        config.readfp(f)

    return config