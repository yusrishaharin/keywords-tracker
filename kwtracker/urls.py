from django.conf.urls import patterns, include, url
from .views import MainView
from django.contrib import admin
import django

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'kwtracker.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # url(r'^admin/', include(admin.site.urls)),

    url(r'^$', MainView.as_view(), name='main'),
)
