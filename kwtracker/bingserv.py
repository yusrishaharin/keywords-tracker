# https://github.com/xthepoet/pyBingSearchAPI

# USAGE
# query_string = "Brad Pitt"
# bing = BingSearchAPI()
# params = {'ImageFilters':'"Face:Face"',
#           '$format': 'json',
#           '$top': 10,
#           '$skip': 0}
# print bing.search('image+web',query_string,params).json()


import requests
import string
from kwtracker import config

ACCOUNT_KEY = config.parse_config().get('bing','account_key')


class BingSearchAPI():
    bing_api = "https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Composite?"

    def replace_symbols(self, request):
        # Custom urlencoder.
        # They specifically want %27 as the quotation which is a single quote '
        # We're going to map both ' and " to %27 to make it more python-esque
        request = string.replace(request, "'", '%27')
        request = string.replace(request, '"', '%27')
        request = string.replace(request, '+', '%2b')
        request = string.replace(request, ' ', '%20')
        request = string.replace(request, ':', '%3a')
        return request

    def search(self, sources, query, params):
        ''' This function expects a dictionary of query parameters and values.
            Sources and Query are mandatory fields.
            Sources is required to be the first parameter.
            Both Sources and Query requires single quotes surrounding it.
            All parameters are case sensitive. Go figure.

            For the Bing Search API schema, go to http://www.bing.com/developers/
            Click on Bing Search API. Then download the Bing API Schema Guide
            (which is oddly a word document file...pretty lame for a web api doc)
        '''
        request = 'Sources="' + sources + '"'
        request += '&Query="' + str(query) + '"'
        for key,value in params.iteritems():
            request += '&' + key + '=' + str(value)
        request = self.bing_api + self.replace_symbols(request)
        return requests.get(request, auth=(ACCOUNT_KEY, ACCOUNT_KEY))
