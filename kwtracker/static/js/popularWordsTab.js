/**
 * Created with PyCharm.
 * User: root
 * Date: 7/27/14
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
kwTracker.controller('popularWordsCtrl', ['$scope',
    function ($scope) {
        'use strict';

        $scope.data = {
            popWords: [
                {text: 'Elon Musk'},
                {text: 'Mars One'},
                {text: 'Justin Bieber'}
            ]
        };
    }
]);

kwTracker.run(['$templateCache', function ($templateCache) {
    'use strict';

    $templateCache.put('popular-words.html',
        '<ion-content direction="x" scrollbar-x="false"> ' +
            '<h2>Popular:</h2> ' +
            '<button class="button button-royal" ng-repeat="word in data.popWords" ' +
            'ng-bind="word.text" ng-class="{\'activated\': word.active}"></button> ' +
        '</ion-content> '
    );




}]);

