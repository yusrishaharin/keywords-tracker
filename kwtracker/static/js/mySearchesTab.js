/**
 * Created with PyCharm.
 * User: root
 * Date: 7/27/14
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
kwTracker.controller('mySearchWordsCtrl', ['$scope', '$ionicPopup', '$timeout',
    function ($scope, $ionicPopup, $timeout) {
        'use strict';

        var me = this;

        $scope.data = {
            myWords: [
                {text: 'Elon Musk', active: true},
                {text: 'Mars One'},
                {text: 'Justin Bieber'}
            ],
            newSearch: null
        };

        me.appendWord = function (word) {
            var obj = { text: word };
            $scope.data.myWords.unshift(obj);

            for (var i=0; i<$scope.data.myWords.length; i++) {
                $scope.data.myWords[i].active = false;
            }
            $scope.data.myWords[0].active = true;
        };

          // Triggered on a button click, or some other target
        $scope.searchWord = function () {
            $scope.data.newSearch = null;

            var myPopup = $ionicPopup.show({
                template: '<input type="text" ng-model="data.newSearch">',
                title: 'Search the Web',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' },
                    {
                        text: '<b>Search</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            if (!$scope.data.newSearch || $scope.data.newSearch.trim() === '') {
                                //don't allow the user to close unless he enters a value
                                e.preventDefault();
                            } else {
                                return $scope.data.newSearch;
                            }
                        }
                    }
                ]
            });

            myPopup.then(function(res) {
                if (res && res.trim() !== '') {
                    me.appendWord(res);
                    //TODO - do search
                }
            });
        };
    }
]);

kwTracker.run(['$templateCache', function ($templateCache) {
    'use strict';

    $templateCache.put('my-search-words.html',

        '<ion-content direction="x" scrollbar-x="false"> ' +
            '<h2>My Searches:</h2>' +
            '<button class="button button-energized" ng-repeat="word in data.myWords" ' +
                'ng-bind="word.text" ng-class="{\'activated\': word.active}"></button>' +
            '<button class="button button-stable icon-left ion-plus" ng-click="searchWord()">Add New</button> ' +
        '</ion-content> '
    );


}]);
