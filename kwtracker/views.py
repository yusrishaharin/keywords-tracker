# -*- coding: utf-8 -*-
# from django.contrib.auth.models import User, Group
# from django.http import HttpResponse
from django.views.generic.base import TemplateView
# from django.views.decorators.csrf import csrf_exempt
from ws4redis.redis_store import RedisMessage
from ws4redis.publisher import RedisPublisher


class MainView(TemplateView):
    template_name = 'main.html'

    def get(self, request, *args, **kwargs):
        welcome = RedisMessage('Hello user!')  # create a welcome message to be sent to everybody
        RedisPublisher(facility='foobar', broadcast=True).publish_message(welcome)
        return super(MainView, self).get(request, *args, **kwargs)