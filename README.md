# README #

# Keyword Tracker #

- A mobile web app that displays the top 10 search result of a keyword and notifies the user when the list gets updated.

**Example Keywords:**

* Mars One
* Elon Musk
* Justin Beiber

### Suggested Technologies: ###
* Bing Search API
* Django - Websockets - Redis https://github.com/jrief/django-websocket-redis
* AngularJS